
# Macros

Le but de ce petit exercice est de démontrer l'utilité et la productivité rendue
possible par les macros dans Vim.

## Énoncé

Considérant le contenu du fichier `macros.tex` :

```tex
\begin{tikzpicture}
  \coordinate (p0) at (0.2,2);
  \coordinate (p1) at (0.4,1);
  \coordinate (p2) at (-0.4,1);
  \coordinate (p3) at (-0.7,0.8);
  \coordinate (p4) at (0,0);
  \coordinate (p5) at (-0.2,-2);
  \coordinate (p6) at (-0.4,-1);
  \coordinate (p7) at (0.7,-0.8);
  \newcommand{\nodes}[1]{
    \foreach \x/\y in {#1} {
        \draw[fill] (\x,\y) circle [radius=0.25ex];
        \foreach \xx/\yy in {#1} {
          \draw[dotted] (\x,\y) -- (\xx,\yy);
        }
    }
  }
  \nodes{}
\end{tikzpicture}
```

On souahite obtenir le contenu suivant :

```tex
\begin{tikzpicture}
  \newcommand{\nodes}[1]{
    \foreach \x/\y in {#1} {
      \draw[fill] (\x,\y) circle [radius=0.25ex];
      \foreach \xx/\yy in {#1} {
        \draw[dotted] (\x,\y) -- (\xx,\yy);
      }
    }
  }
  \nodes{0.2/2, 0.4/1, -0.4/1, -0.7/0.8, 0/0, -0.2/-2, -0.4/-1, 0.7/-0.8}
\end{tikzpicture}
```

Remarquez que la tâche semble être répétitive, c.-à-d. qu'on doit déplacer **un
à un** les couples de nombres (`at (X, Y)`) dans la liste d'arguments à
`\nodes`. Les macros permettent de compléter cette tâche rapidement.

**Indice**: `ma02f(di)?\\nodes/}P0f,s/0f}i, 'aj`

<!-- vim: set sts=4 ts=4 sw=4 tw=80 et :-->

